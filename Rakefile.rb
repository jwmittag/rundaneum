#!/usr/bin/env rake

taskdir = File.expand_path(File.join(File.dirname(__FILE__), 'tasks'))
FileList[File.join(taskdir, '**', '*.rake')].each { |task| load task }
