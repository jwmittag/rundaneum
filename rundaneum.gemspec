#!/usr/bin/env ruby
# vim: fileencoding=UTF-8 ft=ruby syn=ruby ts=2 sw=2 ai eol et si

# Copyright (c) 2009 Jörg W Mittag <mailto:JoergWMittag+Rundaneum@GoogleMail.Com>

projdir = File.dirname __FILE__

require 'rubygems'
require 'date'

SPEC = Gem::Specification.new do |s|
  s.name = 	'rundaneum'
  s.version = 	'0.0.0a'
  s.summary = 	'A Knowledge Management System'
  s.email = 	'Jörg W Mittag <JoergWMittag+Rundaneum@GoogleMail.Com>'
  s.homepage = 	'http://JoergWMittag.GitHub.Com/rundaneum/'
  s.description = 	s.summary
  s.author = 	'Jörg W Mittag'
  s.files = 	Dir[File.join(projdir, '**', '*.*')].reject { |file| file =~ /pkg/ }.collect { |file| file.gsub projdir, '.'}
  s.test_files = 	Dir[File.join(projdir, 'spec', '**', '*_spec.rb')]
  s.add_dependency 'facets', '~> 2.8.0'
  s.add_development_dependency 'rspec', '~> 1.2.9'
end

if __FILE__ == $0
  Gem::manage_gems
  Gem::Builder.new(SPEC).build
end
