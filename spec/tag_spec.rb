#!/usr/bin/env spec

libdir = File.expand_path(File.join(File.dirname(__FILE__), '..', 'lib'))
$LOAD_PATH.unshift libdir unless $LOAD_PATH.include? libdir

begin require 'rubygems'; rescue LoadError
else begin gem 'rspec', '~> 1.1.4'; rescue Gem::LoadError; end end
require 'spec'

require 'tag'

describe Tag do

  it 'should not be valid without a name' do
    lambda { Tag.new }.should raise_error(ArgumentError)
  end

  it 'should be valid with a name' do
    lambda { Tag.new :name }.should_not raise_error
  end

  it 'should have the name it was initialized with' do
    Tag.new(:name).name.should == 'name'
  end

  it 'should return a string for the name, even if initialized differently' do
    Tag.new(:name).name.should == 'name'
    Tag.new(42).name.should == '42'
  end

  it 'should create different Tag objects, if initialized with different names' do
    Tag.new(:name).should_not == Tag.new(:bar)
  end

  it 'should return the same Tag object, if initialized with identical names' do
    Tag.new(:Bar, 'Blah').should be_equal(Tag.new('baR', 'Blub'))
    (tag = Tag.new :name).should == Tag.new(:name)
    tag.should == Tag.new('name')
    tag.should == Tag.new('name', 'Baz')
    Tag.new(:bar, 'Blah').should == Tag.new('bar', 'Blub')
    Tag.new('bar', 'Blah').should == Tag.new('bar', 'Blub')
    tag.should be_eql(Tag.new('name'))
    tag.should be_eql(Tag.new('name', 'Baz'))
    Tag.new(:bar, 'Blah').should be_eql(Tag.new('bar', 'Blub'))
    Tag.new('bar', 'Blah').should be_eql(Tag.new('bar', 'Blub'))
    tag.should be_equal(Tag.new('name'))
    tag.should be_equal(Tag.new('name', 'Baz'))
    Tag.new(:bar, 'Blah').should be_equal(Tag.new('bar', 'Blub'))
    Tag.new('bar', 'Blah').should be_equal(Tag.new('bar', 'Blub'))
  end

  it 'should not have a description' do
    Tag.new(:name).description.should be_nil
  end

  it 'should allow setting a description' do
    lambda { Tag.new(:name).description = 'Description' }.should_not raise_error
  end

  it 'should have a description after one is set' do
    (tag = Tag.new(:name)).description = 'Description'
    tag.description.should == 'Description'
  end

  describe 'with a description' do

    it 'should have a description' do
      Tag.new(:name, 'Description').description.should_not be_nil
    end

    it 'should have the description it was initialized with' do
      Tag.new(:name, 'Description').description.should == 'Description'
    end
  end
end
