#!/usr/bin/env rake

projdir = File.expand_path(File.join(File.dirname(__FILE__), '..'))
$LOAD_PATH.unshift projdir unless $LOAD_PATH.include? projdir

require 'rake/gempackagetask'

load 'rundaneum.gemspec'

Rake::GemPackageTask.new(SPEC) do |pkg|
  pkg.need_zip = 	true
  pkg.need_tar_gz = 	true
  pkg.need_tar_bz2 = 	true
end
