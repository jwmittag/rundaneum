#!/usr/bin/env rake

specdir = File.expand_path(File.join(File.dirname(__FILE__), '..', 'spec'))

begin require 'rubygems'; rescue LoadError
else begin gem 'rspec', '~> 1.1.4'; rescue Gem::LoadError; end end
require 'spec/rake/spectask'
desc 'Run the Specs.'
Spec::Rake::SpecTask.new do |t|
  t.spec_opts = ['--options', File.join(specdir, 'spec.opts')]
  t.spec_files = FileList[File.join(specdir, '*_spec.rb')]
end
