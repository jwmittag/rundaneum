require 'facets/multiton'

class Tag
  include Multiton

  attr_reader :name
  attr_accessor :description

  private

  def initialize name, description = nil
    self.name = name.to_s.dup.freeze
    self.description = description unless description.nil?
  end

  attr_writer :name

  def self.multiton_id name, description = nil
    return name.to_s.downcase
  end
end
